$(document).ready(function () {
  const from = $("#from").datepicker({
      defaultDate: "+1w",
    }),
    to = $("#to").datepicker({
      defaultDate: "+1w",
    }),
    allListElements = $("div");

  listVisible = $("div.posts-block").find(allListElements);

  function addListVisible() {
    listVisible.removeClass("block");
    $("#block").removeClass("active");
    $(listVisible).addClass("list");
    $("#list").addClass("active");
  }
  function addBlockVisible() {
    listVisible.removeClass("list");
    $("#list").removeClass("active");
    $(listVisible).addClass("block");
    $("#block").addClass("active");
  }

  const blockBtn = $("#block").on("click", function () {
    addBlockVisible();
  });

  const listBtn = $("#list").on("click", function () {
    addListVisible();
  });

  $("#close-from").on("click", function () {
    from.datepicker("setDate", "");
  });
  $("#close-to").on("click", function () {
    to.datepicker("setDate", "");
  });
  $("#open-to").on("click", function () {
    to.datepicker("show");
  });
  $("#open-from").on("click", function () {
    from.datepicker("show");
  });

  addBlockVisible();
});
